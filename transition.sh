curl --location --request POST 'https://ryanaquino.atlassian.net/rest/api/2/issue/${CI_MERGE_REQUEST_TITLE}/transitions' \
--header 'Authorization: Basic cnlhbl9hcXVpbm8wMDRAeWFob28uY29tOlJ4dDI4eUd6bGJiYmFHOEZTQjFhRjdBOA==' \
--header 'Content-Type: application/json' \
--header 'Cookie: atlassian.xsrf.token=67c112d5-a12d-494e-864f-2107c6740a8f_6ea5ae7dd0e8ccb4a6f2f10b9f6cc45a432a8835_lin' \
--data-raw '{
    "transition": {
        "id": 41
    }
}'